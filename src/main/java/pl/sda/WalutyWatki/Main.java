package pl.sda.WalutyWatki;

public class Main {
    public static void main(String[] args) {
        WatekEUR watekEUR = new WatekEUR();
        WatekUSD watekUSD = new WatekUSD();
        WatekGBP watekGBP = new WatekGBP();
        WatekCHF watekCHF = new WatekCHF();
        Thread watek1 = new Thread(watekEUR);
        Thread watek2 = new Thread(watekUSD);
        Thread watek3 = new Thread(watekGBP);
        Thread watek4 = new Thread(watekCHF);
        watek1.run();
        watek2.run();
        watek3.run();
        watek4.run();
    }
}
