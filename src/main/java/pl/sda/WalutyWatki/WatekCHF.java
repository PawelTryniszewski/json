package pl.sda.WalutyWatki;

import com.google.gson.Gson;
import pl.sda.Rates;
import pl.sda.Waluty;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class WatekCHF implements Runnable {
    @Override
    public void run() {
        Float aktualnyKursPoKtorymSprzedamWalute = Float.valueOf(0);
        Float kursPoKtorymKupilemMiesiacTemu = Float.valueOf(0);
        Float d;
        try {
            URL url = new URL("http://api.nbp.pl/api/exchangerates/rates/c/chf/2018-08-10/?format=json");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            Object inputLine;
            while ((inputLine = in.readLine()) != null) {
                Waluty.WalutyJson walutyJson = new Gson().fromJson((String) inputLine, Waluty.WalutyJson.class);
                System.out.println(walutyJson.getCode());
                aktualnyKursPoKtorymSprzedamWalute = walutyJson.getRates().get(0).getBid();
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            URL url = new URL("http://api.nbp.pl/api/exchangerates/rates/c/chf/2008-07-17/?format=json");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            Object inputLine;
            while ((inputLine = in.readLine()) != null) {
                Waluty.WalutyJson walutyJson = new Gson().fromJson((String) inputLine, Waluty.WalutyJson.class);
                kursPoKtorymKupilemMiesiacTemu = walutyJson.getRates().get(0).getAsk();
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        d = ((100 / kursPoKtorymKupilemMiesiacTemu) * aktualnyKursPoKtorymSprzedamWalute) - 100;
        if (d >= 0) {

            System.out.printf("Zarobiles %.2f\n", d);
        } else {
            System.out.printf("Straciles %.2f zl\n", d);
        }
    }
}
