package pl.sda;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ChuckNorris {
    public static void main(String[] args) throws IOException, InterruptedException {

        Set<Object> objectHashSet = new HashSet<Object>();

        while (objectHashSet.size() < 10) {
            try {
                URL url = new URL("https://api.chucknorris.io/jokes/random");
                BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    Codebeautify data = new Gson().fromJson(inputLine, Codebeautify.class);

                    objectHashSet.add(data.getValue());
                    //System.out.println(data.getValue());
                    Thread.sleep(1);
                }
                in.close();
            } catch (IllegalStateException e) {

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Thread.sleep(1);
        }
        // System.out.println(objectHashSet);
        List<Object> list = new ArrayList<Object>(objectHashSet);
        for (int i = 0; i < list.size(); i++) {

            System.out.println(list.get(i));
        }


    }

    public static class Codebeautify {
        private Object category = null;
        private String icon_url;
        private String id;
        private String url;
        private String value;

        // Getter Methods

        public Object getCategory() {
            return category;
        }

        public String getIcon_url() {
            return icon_url;
        }

        public String getId() {
            return id;
        }

        public String getUrl() {
            return url;
        }

        public String getValue() {
            return value;
        }

        // Setter Methods

        public void setCategory(String category) {
            this.category = category;
        }

        public void setIcon_url(String icon_url) {
            this.icon_url = icon_url;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

}
