package pl.sda.Koty;
import com.google.gson.Gson;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class Main {
    public static void main(String[] args) throws IOException {

        for (int i = 0; i < 10; i++) {
            Cat cat = getRandomCatUrl();

            BufferedImage image = ImageIO.read(new URL(cat.catUrl));

            System.out.println("Resolution: " + image.getWidth() + "x" + image.getHeight());

            int lastDotIndex = cat.catUrl.lastIndexOf('.');
            String extension = cat.catUrl.substring(lastDotIndex + 1);

            File targetFile = new File("src/main/resources/losowyKot_" + i + "." + extension);

            ImageIO.write(image, extension, targetFile);

            System.out.println("Rozmiar: " + targetFile.length()/1024 + " KB");
        }

    }


    private static Cat getRandomCatUrl() throws IOException {
        URL url = new URL("https://aws.random.cat/meow");

        URLConnection connection = url.openConnection();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            String jsonText = br.readLine();
            System.out.println(jsonText);

            Cat cat = new Gson().fromJson(jsonText, Cat.class);
            System.out.println(cat.catUrl);
            return cat;
        }
    }
    public class Cat {
        private String catUrl;

        public String getFile() {
            return catUrl;
        }


        public void setFile(String file) {
            this.catUrl = file;
        }
    }
}