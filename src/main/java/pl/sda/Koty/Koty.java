package pl.sda.Koty;

import com.google.gson.Gson;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Objects;

public class Koty {
    public static void main(String[] args) throws InterruptedException {
        File file = new File("D:\\Projekty\\JSonZadania\\Koty");
        int i = 0;

        while (Objects.requireNonNull(file.listFiles()).length < 10) {
            try {
                URL url = new URL("https://aws.random.cat/meow");
                BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    JSON json = new Gson().fromJson(inputLine, JSON.class);
                    Thread.sleep(1);
                    BufferedImage image = ImageIO.read(new URL(json.getFile()));
                    System.out.println(json.getFile());
                    if (json.getFile().endsWith("jpg")) {
                        ImageIO.write(image, "jpg", new File(file + "\\koty" + String.valueOf(i) + ".jpg"));

                    }else if (json.getFile().endsWith("gif")){
                        ImageIO.write(image, "gif", new File(file + "\\koty" + String.valueOf(i) + ".gif"));
                    }else if (json.getFile().endsWith("jpeg")){
                        ImageIO.write(image, "jpeg", new File(file + "\\koty" + String.valueOf(i) + ".jpeg"));
                    }
                    i++;

                }

                in.close();
            } catch (IllegalStateException ignored) {

            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
            Thread.sleep(1);
        }
    }

    public class JSON {
        private String file;

        public String getFile() {
            return file;
        }


        public void setFile(String file) {
            this.file = file;
        }
    }
}
