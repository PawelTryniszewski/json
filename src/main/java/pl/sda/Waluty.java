package pl.sda;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

public class Waluty {
    public static void main(String[] args) {

        Double tempActual = 0.0;
        Double tempMonthAgo = 0.0;
        Double d;

        try {
            URL url = new URL("http://api.nbp.pl/api/exchangerates/rates/c/usd/2018-08-10/?format=json");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            Object inputLine;
            while ((inputLine = in.readLine()) != null) {
                WalutyJson walutyJson = new Gson().fromJson((String) inputLine, WalutyJson.class);
                System.out.println(walutyJson.getCode());
                System.out.println(walutyJson.getCurrency());
                System.out.println(walutyJson.getRates());
                String a = String.valueOf(walutyJson.getRates());
                tempActual = 100 / Double.valueOf(a.substring(a.length() - 8, a.length() - 2));
                System.out.printf("100PLN = %.2f " + walutyJson.getCode() + "\n", tempActual);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            URL url = new URL("http://api.nbp.pl/api/exchangerates/rates/c/usd/2018-07-10/?format=json");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            Object inputLine;
            while ((inputLine = in.readLine()) != null) {
                WalutyJson walutyJson = new Gson().fromJson((String) inputLine, WalutyJson.class);
                String a = String.valueOf(walutyJson.getRates());
                tempMonthAgo = 100 / Double.valueOf(a.substring(a.length() - 8, a.length() - 2));
                System.out.printf("100PLN = %.2f " + walutyJson.getCode() + "\n", tempMonthAgo);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
         d = tempActual - tempMonthAgo;
        if (d >= 0) {

            System.out.printf("Zarobiles po miesiacu %.2f\n", d);
        }else{
            System.out.printf("Straciles po miesiacu %.2f\n", d);
        }


        try {
            URL url = new URL("http://api.nbp.pl/api/exchangerates/rates/c/chf/2018-08-10/?format=json");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            Object inputLine;
            while ((inputLine = in.readLine()) != null) {
                WalutyJson walutyJson = new Gson().fromJson((String) inputLine, WalutyJson.class);
                System.out.println(walutyJson.getCode());
                System.out.println(walutyJson.getCurrency());
                System.out.println(walutyJson.getRates());
                String a = String.valueOf(walutyJson.getRates());
                tempActual = 100 / Double.valueOf(a.substring(a.length() - 8, a.length() - 2));
                System.out.printf("100PLN = %.2f " + walutyJson.getCode() + "\n", tempActual);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            URL url = new URL("http://api.nbp.pl/api/exchangerates/rates/c/chf/2018-07-10/?format=json");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            Object inputLine;
            while ((inputLine = in.readLine()) != null) {
                WalutyJson walutyJson = new Gson().fromJson((String) inputLine, WalutyJson.class);
                String a = String.valueOf(walutyJson.getRates());
                tempMonthAgo = 100 / Double.valueOf(a.substring(a.length() - 8, a.length() - 2));
                System.out.printf("100PLN = %.2f " + walutyJson.getCode() + "\n", tempMonthAgo);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

         d = tempActual - tempMonthAgo;
        if (d >= 0) {

            System.out.printf("Zarobiles po miesiacu %.2f\n", d);
        }else{
            System.out.printf("Straciles po miesiacu %.2f\n", d);
        }

        try {
            URL url = new URL("http://api.nbp.pl/api/exchangerates/rates/c/gbp/2018-08-10/?format=json");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            Object inputLine;
            while ((inputLine = in.readLine()) != null) {
                WalutyJson walutyJson = new Gson().fromJson((String) inputLine, WalutyJson.class);
                System.out.println(walutyJson.getCode());
                System.out.println(walutyJson.getCurrency());
                System.out.println(walutyJson.getRates());
                String a = String.valueOf(walutyJson.getRates());
                tempActual = 100 / Double.valueOf(a.substring(a.length() - 8, a.length() - 2));
                System.out.printf("100PLN = %.2f " + walutyJson.getCode() + "\n", tempActual);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            URL url = new URL("http://api.nbp.pl/api/exchangerates/rates/c/gbp/2018-07-10/?format=json");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            Object inputLine;
            while ((inputLine = in.readLine()) != null) {
                WalutyJson walutyJson = new Gson().fromJson((String) inputLine, WalutyJson.class);
                String a = String.valueOf(walutyJson.getRates());
                tempMonthAgo = 100 / Double.valueOf(a.substring(a.length() - 8, a.length() - 2));
                System.out.printf("100PLN = %.2f " + walutyJson.getCode() + "\n", tempMonthAgo);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        d = tempActual - tempMonthAgo;
        if (d >= 0) {

            System.out.printf("Zarobiles po miesiacu %.2f\n", d);
        }else{
            System.out.printf("Straciles po miesiacu %.2f\n", d);
        }
        try {
            URL url = new URL("http://api.nbp.pl/api/exchangerates/rates/c/eur/2018-08-10/?format=json");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            Object inputLine;
            while ((inputLine = in.readLine()) != null) {
                WalutyJson walutyJson = new Gson().fromJson((String) inputLine, WalutyJson.class);
                System.out.println(walutyJson.getCode());
                System.out.println(walutyJson.getCurrency());
                System.out.println(walutyJson.getRates());
                String a = String.valueOf(walutyJson.getRates());
                tempActual = 100 / Double.valueOf(a.substring(a.length() - 8, a.length() - 2));
                System.out.printf("100PLN = %.2f " + walutyJson.getCode() + "\n", tempActual);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            URL url = new URL("http://api.nbp.pl/api/exchangerates/rates/c/eur/2018-07-10/?format=json");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            Object inputLine;
            while ((inputLine = in.readLine()) != null) {
                WalutyJson walutyJson = new Gson().fromJson((String) inputLine, WalutyJson.class);
                String a = String.valueOf(walutyJson.getRates());
                tempMonthAgo = 100 / Double.valueOf(a.substring(a.length() - 8, a.length() - 2));
                System.out.printf("100PLN = %.2f " + walutyJson.getCode() + "\n", tempMonthAgo);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        d = tempActual - tempMonthAgo;
        if (d >= 0) {

            System.out.printf("Zarobiles po miesiacu %.2f\n", d);
        }else{
            System.out.printf("Straciles po miesiacu %.2f\n", d);
        }
    }

    public static class WalutyJson {

        private Object table, ratess;
        private String code, currency;
        private double topCount;
        private Object date, startDate, endDate;
        ArrayList < Rates> rates = new ArrayList<>();

        public Object getRatess() {
            return ratess;
        }
        public ArrayList<Rates> getRates (){
            return this.rates;
        }

        public void setRates(ArrayList<Rates> rates) {
            this.rates = rates;
        }

        public Object getTable() {
            return table;
        }

        public void setTable(Object table) {
            this.table = table;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public double getTopCount() {
            return topCount;
        }

        public void setTopCount(double topCount) {
            this.topCount = topCount;
        }

        public Object getDate() {
            return date;
        }

        public void setDate(Object date) {
            this.date = date;
        }

        public Object getStartDate() {
            return startDate;
        }

        public void setStartDate(Object startDate) {
            this.startDate = startDate;
        }

        public Object getEndDate() {
            return endDate;
        }

        public void setEndDate(Object endDate) {
            this.endDate = endDate;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }


        public void setRatess(Object rates) {
            this.ratess = rates;
        }

        public void setRatess(String rates) {
            this.ratess = rates;
        }
    }
}
